import 'bootstrap';
import './scss/app.scss';
import $ from 'jquery';
window.jQuery = $;
window.$ = $;

var total = 0;
var last_operation = '';
var last_number = 0;
var screen = $("#screen");
var operation = $(".operation");
operation.prop('disabled', true);

(function( $ ){
    $.fn.operation = (oper) => {
        last_operation = oper;
        total = parseFloat(screen.val());
        screen.val(0);
        operation.prop('disabled', true);
        return true;
    };
    $.fn.buttonOperation = (value) => {
        screen.val(parseFloat(screen.val() + value));
        last_number = parseFloat(screen.val());
        operation.prop('disabled', false);
        return true;
    };
})($);

$("#clear").on("click", function(event) {
    total = last_number = 0;
    last_operation = '';
    screen.val(0);
    operation.prop('disabled', true);
});

$("#equal").on("click",(event) => {
    if(last_operation === 'plus')
    {
        total += last_number;
    }
    if(last_operation === 'less')
    {
        total -= last_number;
    }
    if(last_operation === 'multiply')
    {
        total = (total || 1) * last_number;
    }
    if(last_operation === 'divide')
    {
        total = total/last_number;
    }
    if(last_operation)
    {
        screen.val(total);
    }
});

$("#button1").on("click", function(event) {
    $(this).buttonOperation('1');
});

$("#button2").on("click",(event) => {
    $(this).buttonOperation('2');
});

$("#button3").on("click",(event) => {
    $(this).buttonOperation('3');
});

$("#button4").on("click",(event) => {
    $(this).buttonOperation('4');
});

$("#button5").on("click",(event) => {
    $(this).buttonOperation('5');
});

$("#button6").on("click",(event) => {
    $(this).buttonOperation('6');
});

$("#button7").on("click",(event) => {
    $(this).buttonOperation('7');
});

$("#button8").on("click",(event) => {
    $(this).buttonOperation('8');
});

$("#button9").on("click",(event) => {
    $(this).buttonOperation('9');
});

$("#button0").on("click",(event) => {
    $(this).buttonOperation('0');
});

$("#plus").on("click", (event) => {
    $(this).operation('plus');
});

$("#less").on("click",(event) => {
    $(this).operation('less');
});

$("#multiply").on("click",(event) => {
    $(this).operation('multiply');
});

$("#divide").on("click",(event) => {
    $(this).operation('divide');
});

$("#dot").on("click",(event) => {
    screen.val(screen.val() + '.');
});

$(document).bind('keydown', function(e) {
    //0
    if (e.which === 48 || e.which === 96) {
        $("#button0").trigger('click');
    }
    //1
    if (e.which === 49 || e.which === 97) {
        $("#button1").trigger('click');
    }
    //2
    if (e.which === 50 || e.which === 98) {
        $("#button2").trigger('click');
    }
    //3
    if (e.which === 51 || e.which === 99) {
        $("#button3").trigger('click');
    }
    //4
    if (e.which === 52 || e.which === 100) {
        $("#button4").trigger('click');
    }
    //5
    if (e.which === 53 || e.which === 101) {
        $("#button5").trigger('click');
    }
    //6
    if (e.which === 56 || e.which === 102) {
        $("#button6").trigger('click');
    }
    //7
    if (e.which === 57 || e.which === 103) {
        $("#button7").trigger('click');
    }
    //8
    if (e.which === 58 || e.which === 104) {
        $("#button8").trigger('click');
    }
    //9
    if (e.which === 59 || e.which === 105) {
        $("#button9").trigger('click');
    }
    //+
    if (e.which === 107 && operation.prop('disabled') === false) {
        $("#plus").trigger('click');
    }
    //-
    if (e.which === 109 && operation.prop('disabled') === false) {
        $("#less").trigger('click');
    }
    //*
    if (e.which === 106 && operation.prop('disabled') === false) {
        $("#multiply").trigger('click');
    }
    //div
    if (e.which === 111 && operation.prop('disabled') === false) {
        $("#divide").trigger('click');
    }
    //equal
    if (e.which === 13) {
        $("#equal").trigger('click');
    }
    //equal
    if (e.which === 8) {
        $("#clear").trigger('click');
    }
    //dot
    if (e.which === 110) {
        $("#dot").trigger('click');
    }
});